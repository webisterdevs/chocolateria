<!DOCTYPE html>
<html>

<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>

<body>
<div id="fundo-outras">

    <?php
        include 'menu.php';
    ?>

        <section id="corpo-lojas">
        <h1>O VERDADEIRO SABOR DA PÁSCOA 2016</h1>

        <h4>
            <small><small><small>
                <a href="#linha-classicos"><u>Linha Classicos</u></a>
                /
                <a href="#linha-gourmet"><u>Linha Gourmet</u></a>
                /
                <a href="#linha-especiais"><u>Linha Especiais</u></a>
            </small></small></small>
        </h4>
        <br>
        <a name="linha-classicos">
            <table class="tabela-pascoa" border="0">
                <tr>
                    <td colspan="2">
                        <h1>Linha Clássicos</h1>
                        <br><br><br>
                    </td>
                </tr>
                <tr>
                    <td>
                        <img src="_img/ovo-de-pascoa-ao-leite.png">
                    </td>
                    <td>
                        <h4><big>Ovos Clássicos 295gr</big></h4>
                        <h2>
                            Os clássicos e mais finos chocolates Chocolateria Brasileira em ovos de chocolate, recheados com tabletes no mesmo sabor.
                            <br><br>
                            Disponíveis nos sabores:
                            <br>
                            - Ao Leite
                            <br>
                            - Meio a Meio (Ao leite e Branco)
                            <br>
                            - Meio Amargo
                        </h2>
                    </td>
                </tr>
            </table>
        </a>
        <br>
        <div class="linha-gourmet">
            <a name="linha-gourmet">
                <br>
                <h1><b>Linha Gourmet</b></h1>
                <br><br>
                <table class="tabela-pascoa" border="0">
                    <tr>
                        <td>
                            <h4><big>Ovos Trufados 400gr</big></h4>
                            <h6>
                                Incrivelmente saborosos, nossos ovos trufados surpreendem pela generosa camada de recheio e sabores inspirados nas receitas exclusivas de nossas trufas e bombons.
                                A embalagem diferenciada completa todo o glamour da linha Gourmet de Páscoa da Chocolateria Brasileira.
                                <br><br>
                                Disponíveis nos sabores:
                                <br>
                                - Tradicional
                                <br>
                                - Maracujá
                                <br>
                                - Coco
                                <br>
                                -Frutas Vermelhas
                            </h6>
                        </td>
                        <td>
                            <img src="_img/ovo-trufado.png">
                        </td>
                    </tr>
                </table>
                <br>
                <table class="tabela-pascoa" border="0">
                    <tr>
                        <td>
                            <img src="_img/ovo-colher-delicia.png">

                        </td>
                        <td>
                            <h4><big>Ovos de Colher 400gr</big></h4>
                            <h2>
                                Se destaca pela elegância na apresentação e <b>sabor incomparável</b>. <br>O produto mais esperado e desejado de nossa linha de páscoa pelo terceiro ano consecutivo.
                            </h2>
                        </td>
                    </tr>
                </table>
            </a>
            <a href="#"><h4><small><small>voltar ao topo</small></small></h4></a><br>
        </div>

        <div class="linha-especiais">
            <a name="linha-especiais">
                <h1><b>Linha Especiais</b></h1>
                <table class="tabela-pascoa">
                    <tr>
                        <td>
                            <h4><big>Ovos Especiais 320g</big></h4>
                            <h6>
                                Ovos de páscoa diferenciados, que combinam os melhores ingredientes com a delicadeza das embalagens. Presente para todos os gostos.
                                <br><br>
                                Disponíveis nos sabores:
                                <br>
                                - Ao leite com bombons cereja ao licor
                                <br>
                                - Ao leite Felino
                                <br>
                                - Branco com cookies
                            </h6>
                        </td>
                        <td>
                            <img src="_img/ovo-especial.png">
                        </td>
                    </tr>
                </table>
                <br><br>
                <table class="tabela-pascoa">
                    <tr>
                        <td>
                            <img src="_img/ovo-maxi-trufa.png">
                        </td>
                        <td>
                            <h4><big>Ovos Maxi Trufado 600gr</big></h4>
                            <h2>
                                Ovos ao leite com uma quantidade generosa de bombons trufados sortidos. Para quem é apaixonado por chocolate.
                            </h2>
                        </td>
                    </tr>
                </table>
            </a>
            <br><br>
            <a href="#"><h4><small><small>voltar ao topo</small></small></h4></a><br>
            <div class="catalogo">
                <h4><a href="catalogo_pascoa.pdf" target="_blank">Confira o nosso catálogo</a> com a linha completa!</h4>
            </div>

            <br><br>
        </div>
            <!--
        <div class="linha-gourmet">
        <a name="linha-infantil">
            <h1><b>Linha Infantil</b></h1>
            <table>
                <tr>
                    <td>

                    </td>
                </tr>
            </table>
        </a>
        </div>

        <a name="linha-zero">
            <h1><b>Linha Zero</b></h1>
            <table>
                <tr>
                    <td>

                    </td>
                </tr>
            </table>
        </a>

        -->

    </section>

</div><br><br>

<?php
    include 'rodape.php';
?>

</body>
</html>