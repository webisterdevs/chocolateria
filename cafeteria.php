<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" href="_css/form.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <link rel="icon" href="_img/favicon.png">
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
        include 'menu.php';
    ?>

    <section id="corpo-lojas">
        <h1>Cafeteria</h1>
        <h4><small><small>A Chocolateria Brasileira conta com uma Sofisticada área de Café <br>Com um Cardápio Rico e Único, Excelente para o seu momento de lazer.</small></small></h4>

        <?php
            include 'slider_cafe.php';
        ?>

        <h4><small><small>Confira o Nosso <a href="cardapio_site.pdf" target="_blank"><u>Cardápio Completo</u></a></small></small></h4>
        <br><br>
    </section>


</div><br><br>

<?php
    include 'rodape.php';
?>

</body>
</html>