<?php

/* Medida preventiva para evitar que outros dom�nios sejam remetente da sua mensagem. */
if (eregi('tempsite.ws$|chocolateriabrasileira.com.br$|hospedagemdesites.ws$|websiteseguro.com$', $_SERVER[HTTP_HOST])) {
    $emailsender='simone@chocolateriabrasileira.com.br'; // Substitua essa linha pelo seu e-mail@seudominio
} else {
    $emailsender = "simone@" . $_SERVER[HTTP_HOST];
    //    Na linha acima estamos for�ando que o remetente seja 'webmaster@seudominio',
    // Voc� pode alterar para que o remetente seja, por exemplo, 'contato@seudominio'.
}

/* Verifica qual �o sistema operacional do servidor para ajustar o cabe�alho de forma correta.  */
if(PATH_SEPARATOR == ";") $quebra_linha = "\r\n"; //Se for Windows
else $quebra_linha = "\n"; //Se "não for Windows"

// Passando os dados obtidos pelo formul�rio para as vari�veis abaixo
$nomeremetente     = $_POST['tNome'];
$tel               = $_POST['tTel'];
$emailremetente    = $_POST['tMail'];
$profissao         = $_POST['tProf'];
$idade             = $_POST['tIdade'];
$cidade            = $_POST['tCidade'];
$valor             = $_POST['cValor'];
$comcopia          = $_POST['comcopia'];
$comcopiaoculta    = $_POST['comcopiaoculta'];
$assunto           = "Contato do site - Franqueado";
$mensagem          = $_POST['tMsg'];


/* Montando a mensagem a ser enviada no corpo do e-mail. */
$mensagemHTML = '<P>Contato realizado atraves do Site.</P>
<P><b>Cliente: </b>'.$nomeremetente.'</P>
<p><b>Telefone: </b>'.$tel.'</p>
<p><b>Email: </b>'.$emailremetente.'</p>
<p><b>Profissão: </b>'.$profissao.'</p>
<p><b>Idade: </b>'.$idade.'</p>
<p><b>Cidade de Interesse: </b>'.$cidade.'</p>
<p><b>Valor para Investimento: </b>'.$valor.'</p>
<p><b>Mensagem: </b></p>
<p><b><i>'.$mensagem.'</i></b></p>
<hr>';


/* Montando o cabeçalho da mensagem */
$headers = "MIME-Version: 1.1" .$quebra_linha;
$headers .= "Content-type: text/html; charset=UTF-8" .$quebra_linha;
// Perceba que a linha acima cont�m "text/html", sem essa linha, a mensagem n�o chegar� formatada.
$headers .= "From: " . $emailsender.$quebra_linha;
//$headers .= "Cc: " . $comcopia . $quebra_linha;
//$headers .= "Bcc: " . $comcopiaoculta . $quebra_linha;
$headers .= "Reply-To: " . $emailremetente . $quebra_linha;
// Note que o e-mail do remetente ser� usado no campo Reply-To (Responder Para)

/* Enviando a mensagem */

//� obrigat�rio o uso do par�metro -r (concatena��o do "From na linha de envio"), aqui na Locaweb:

if ($emailremetente=='')
{
    $msg = "Favor preencher campo Email";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($nomeremetente=='') {
    $msg = "Favor preencher campo Nome";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($mensagem=='') {
    $msg = "Favor preencher campo Mensagem";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($tel=='') {
    $msg = "Favor preencher campo Telefone";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($profissao=='') {
    $msg = "Favor preencher campo Profissao";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($idade=='') {
    $msg = "Favor preencher campo Idade";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($cidade=='') {
    $msg = "Favor preencher campo Cidade";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif ($valor=='') {
    $msg = "Favor preencher campo Valor";
    echo "<script>location.href='../contato.php'; alert('$msg');</script>";
}

elseif(!mail($emailsender, $assunto, $mensagemHTML, $headers ,"-r".$emailsender)){ // Se for Postfix
    $headers .= "Return-Path: " . $emailsender . $quebra_linha; // Se "n�o for Postfix"
    mail($emailsender, $assunto, $mensagemHTML, $headers );


}

/*
/* Mostrando na tela as informa��es enviadas por e-mail
print "Mensagem <b>$assunto</b> enviada com sucesso!<br><br>
De: $emailsender<br>
Para: $emailsender<br>
<p><a href='".$_SERVER["HTTP_REFERER"]."'>Voltar</a></p>"

*/

$msg = "Sua Mensagem foi enviada com Sucesso.";
echo "<script>location.href='../contato.php'; alert('$msg');</script>";

?>