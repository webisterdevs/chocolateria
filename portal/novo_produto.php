<?php
include "conectar_banco.php";
include "insere-log.php";
include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
protegePagina();

$sql = mysql_query("SELECT permissao FROM usuarios where id = " . $_SESSION['usuarioID']);
$resultado = mysql_fetch_assoc($sql);

if ($resultado['permissao'] == 0) {
	$cabecalho = "header-noPermission.php";
} elseif ($resultado['permissao'] == 2) {
	$cabecalho = "header-master.php";
} else {
	$cabecalho = "header.html";
}

require $cabecalho;

$produto = mysql_query("SELECT * FROM produtos");

?>
<script type="text/javascript">
	$(function(){
    $(".target-active").find("[href='novo_produto.php']").parent().addClass("active");
});
</script>

        <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
        	<h1 class="page-header">Novo Produto</h1>
        	<form class="form-horizontal" name="usuario" method="post" action="insere_produto.php">
	          <div class="form-group">
	              <label for="cDescricao" class="col-sm-2 control-label">Produto</label>
	              <div class="col-sm-3">
	                <input class="form-control" type="text" placeholder="Nome" id="cDescricao" name="cDescricao">
	              </div>
	          </div>
	          <div class="form-group">
	              <label for="valorCaixa" class="col-sm-2 control-label">R$ Caixa</label>
	              <div class="col-sm-3">
	                <input class="form-control" type="text" placeholder="Valor da Caixa" id="valorCaixa" name="valorCaixa">
	              </div>
	          </div>
	          <div class="form-group">
	              <label for="valorUnitario" class="col-sm-2 control-label">R$ Unitário</label>
	              <div class="col-sm-3">
	                <input class="form-control" type="text" placeholder="Valor Unitário" id="valorUnitario" name="valorUnitario">
	              </div>
	          </div>
				  <div class="form-group">
				    <div class="col-sm-offset-2 col-sm-10">
				      <button type="submit" class="btn btn-primary">Cadastrar</button>
				    </div>
				  </div>
	      </form>
	      <br>
        	<h3 class="sub-header">Gerenciar Produtos</h3>
        	<div class="table-responsive">
        		<br>
        		<table class="table">
        		<thead>
					<tr>
						<th>ID</th>
						<th>Produto</th>
				        <th>Valor da Caixa</th>
						<th>Valor Unitário</th>
						<th>Ações</th>
					</tr>
				</thead>
				<?php
					while ($ver=mysql_fetch_array($produto)) {
						echo "<tr>";
						echo "<td>".$ver['id_produto']."</td>";
						echo "<td>".$ver['descricao']."</td>";
						echo "<td>".$ver['valor_caixa']."</td>";
						echo "<td>".$ver['valor_unitario']."</td>";
						echo "<td><a href='alterar-produto.php?prod=".$ver['descricao']."&caixa=".$ver['valor_caixa']."&unit=".$ver['valor_unitario']."&id=".$ver['id_produto']."'><i class='fa fa-pencil' aria-hidden='true'></i></a>&nbsp;&nbsp;<a href='del_produto.php?id=".$ver['id_produto']."'><i class='fa fa-trash' aria-hidden='true'></i></a></td>";
						
					}
					echo $popup;
				?>
        		</table>
        	</div>
        </div>



<?php
	require "footer.php";
?>