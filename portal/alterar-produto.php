
<?php
include "conectar_banco.php";
include "insere-log.php";
include("seguranca.php"); // Inclui o arquivo com o sistema de segurança
protegePagina();

$cabecalho = "header.html";
$sql = mysql_query("SELECT permissao FROM usuarios where id = " . $_SESSION['usuarioID']);
$resultado = mysql_fetch_assoc($sql);

if ($resultado['permissao'] == 0) {
$cabecalho = "header-noPermission.php";
}

require $cabecalho;

$prod = $_GET['prod'];
$caixa = $_GET['caixa'];
$unit = $_GET['unit'];
$id = $_GET['id'];

?>
<script type="text/javascript">
    $(function(){
    $(".target-active").find("[href='novo_produto.php']").parent().addClass("active");
});
</script>

<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
            <h1 class="page-header">Alterar Produto</h1>
            <h5><b>Nome:</b> <?php echo $prod; ?></h5>
            <h5><b>R$ Caixa:</b> <?php echo $caixa; ?></h5>
            <h5><b>R$ Unitário:</b> <?php echo $unit; ?></h5>
            <div class="col-sm-4"><i><small>*Deixe em branco os campos que não deseja alterar</small></i></div>
            <br>
            <br>
            <br>
            <form class="form-horizontal" name="usuario" method="post" action="altera_produto.php">
              <div class="form-group">
                  <label for="cDescricao" class="col-sm-2 control-label">Nome</label>
                  <div class="col-sm-4">
                    <input class="form-control" type="text" placeholder="Alterar Nome" id="cDescricao" name="cDescricao">
                  </div>
              </div>
              <?php echo "<input type='hidden' name='id' value=".$id.">"; ?>
              <div class="form-group">
                  <label for="valorCaixa" class="col-sm-2 control-label">R$ Caixa</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="text" placeholder="Alterar Valor da Caixa" id="valorCaixa" name="valorCaixa">
                  </div>
              </div>
              <div class="form-group">
                  <label for="valorUnitario" class="col-sm-2 control-label">R$ Unitário</label>
                  <div class="col-sm-3">
                    <input class="form-control" type="text" placeholder="Alterar Valor Unitário" id="valorUnitario" name="valorUnitario">
                  </div>
              </div>
                  <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                      <button type="submit" class="btn btn-primary">Alterar</button>
                      <a href="novo_produto.php" class="btn btn-default">Voltar</a>
                    </div>
                  </div>
          </form>

<?php
    require "footer.php";
?>