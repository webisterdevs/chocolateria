<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="bombom-e-trufa.php">Trufas e Bombons</a> > Trufa Artesanal 150gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/trufa-artesanal.png">
                </td>
                <td>
                    <h1><big>Trufa Artesanal 150gr :</big></h1>
                    <h2>A Chocolateria Brasileira desenvolveu em parceria com o Chef Chocolatier Renato Pereira uma receita exclusiva, baseada na técnica francesa de produção de trufas artesanais, onde a mistura de sabores entre a massa interna macia  e a casquinha fina de chocolate de diferentes sabores traz a esse produto um sabor único e incomparável.<br>
                        Produzidas uma a uma é o que dá origem ao seu formato irregular e transforma esse produto em um dos mais charmosos presentes da marca.
                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>