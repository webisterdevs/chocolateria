<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>


    <section id="corpo-lojas">
        <h1>Trufas e Bombons</h1>

        <ul class="albun-fotos2">
            <a href="bombom-trufado-15gr.php"><li id="foto07"><span>Bombom Trufado 15gr</span></li></a>
            <a href="trufa-artesanal.php"><li id="foto08"><span>Trufa Artesanal 150gr</span></li></a>
            <a href="trufa27gr.php"><li id="foto09"><span><small>Trufa 27gr</small></span></li></a>
        </ul>
        <ul class="albun-fotos2">
            <a href="pirulito-confeitos.php"><li id="foto10"><span>Pirulito com confeitos 30gr</span></li></a>
            <a href="carrinhos-ursinhos.php"><li id="foto11"><span>Carrinho / Ursinho / Coração ao leite 70gr</span></li></a>
            <a href="brigadeiro-palito.php"><li id="foto12"><span>Brigadeiro no Palito 30gr</span></li></a>
        </ul>

        <!--
        <table border="0" width="80%" id="produtos">
            <tr>
                <td align="center">
                    <a href="bombom-trufado-15gr.php"><img src="_img/trufa-cereja.png"></a>
                </td>
                <td>
                    <a href="trufa-artesanal.php"><img src="_img/trufa-artesanal.png"></a>
                </td>
                <td>
                    <a href="trufa27gr.php"><img src="_img/trufas-27gr.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Bombom Trufado 15gr</h4>
                </td>
                <td>
                    <h4>Trufa Artesanal 150gr</h4>
                </td>
                <td>
                    <h4>Trufa 27 gr</h4>
                </td>
            </tr>
            <tr>
                <td align="center"><br><br><br><br><br><br><br>
                    <a href="pirulito-confeitos.php"><img src="_img/pirulito-confeitos.png"></a>
                </td>
                <td valign="bottom">
                    <a href="carrinhos-ursinhos.php"><img src="_img/carrinhos-ursinhos.png"></a>
                </td>
                <td valign="bottom">
                    <a href="brigadeiro-palito.php"><img src="_img/brigadeiro-palito.png"></a>
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Pirulito com confeitos 30gr</h4>
                </td>
                <td>
                    <h4>Carrinho / Ursinho / Coração ao leite 70gr</h4>
                </td>
                <td>
                    <h4>Brigadeiro no Palito 30gr</h4>
                </td>
            </tr>
        </table>
        -->
    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>