
<form method="post" id="fContato" action="lw/envia2.php">
    <fieldset id="usuario"><legend>Seja Franqueado</legend>
        <br>
        <p><label for="cNome">Nome Completo*</label><br><input type="text" name="tNome" id="cNome" size="25" maxlength="50"/></p>
        <p><label for="cMail">E-mail*</label><br><input type="email" name="tMail" id="cMail" size="25" maxlength="50"/></p>
        <p><label for="cTel">Telefone</label><br><input type="tel" name="tTel" id="cTel" size="25" maxlength="18" placeholder="(0xx) + número"/></p>
        <p><label for="cProf">Profissão</label><br><input type="text" name="tProf" id="cProf" size="25" maxlength="35"></p>
        <p><label for="cIdade">Idade</label><br><input type="text" name="tIdade" id="cIdade" size="5" maxlength="2"></p>
        <p><label for="cCidade">Cidade de Interesse</label><br><input type="text" name="tCidade" id="cCidade" size="25" maxlength="35"></p>
        <p><label for="cValor">Valor para Investimento</label><br>
            <select name="cValor" id="cValor">
                <option>Selecione...</option>
                <option>De R$100,000 até R$150,000</option>
                <option>De R$151,000 até R$250,000</option>
                <option>Acima de R$250,000</option>
            </select>
        </p>
        <p><label for="cMsg">Mensagem*</label><br><textarea name="tMsg" id="cMsg" cols="55" rows="5" placeholder="Deixe aqui sua mensagem"></textarea></p>
        <input type="submit" value="Enviar" id="botao"/>
        <h3>*Preenchimento Obrigatório</h3>
    </fieldset><br>
</form>