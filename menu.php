<div class="content">
    <h1 class="title"> <img src="_img/logo_final_2016_peq2.png"> <span><small>CHOCOLATERIA BRASILEIRA</small></span></h1>
    <ul id="sdt_menu" class="sdt_menu">
        <li>
            <a href="index.html">
                <img src="images/fundo-home.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Home</span>
							<span class="sdt_descr">Página Inicial</span>
						</span>
            </a>
        </li>
        <li>
            <a href="produtos.php">
                <img src="images/1.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Produtos</span>
							<span class="sdt_descr">EM BREVE!</span>
						</span>
            </a>
           <!-- <div class="sdt_box">
                <a href="dia-das-maes.php">Dia das Mães</a>
                <a href="barras.php">Barras</a>
                <a href="bombom-e-trufa.php">Bombons / Trufas</a>
                <a href="presentes.php">Presentes</a>
                <a href="#">Granel</a>
            </div>-->
        </li>
        <li>
            <a href="franquias.php">
                <img src="images/2.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Nossas Lojas</span>
							<span class="sdt_descr">Encontre-nos</span>
						</span>
            </a>
        </li>
        <li>
            <a href="sobre-nos.php">
                <img src="images/5.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Sobre Nós</span>
							<span class="sdt_descr">A Chocolateria Brasileira...</span>
						</span>
            </a>
        </li>
        <li>
            <a href="cafeteria.php">
                <img src="images/4.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Cafeteria</span>
							<span class="sdt_descr">O que temos no cardápio</span>
						</span>
            </a>
            <!--<div class="sdt_box">
                <a href="sobre-nos.php">Cafés Quentes</a>
                <a href="sobre-nos.php">Cafés Gelados</a>
                <a href="sobre-nos.php">Sobremesas</a>
                <a href="sobre-nos.php">Salgados</a>
            </div>-->
        </li>
        <li>
            <a href="contato.php">
                <img src="images/7.png" alt=""/>
                <span class="sdt_active"></span>
						<span class="sdt_wrap">
							<span class="sdt_link">Contato</span>
							<span class="sdt_descr">Fale Conosco!</span>
						</span>
            </a>
            <!--<div class="sdt_box">
                <a href="contato.php">Contato</a>
                <a href="seja-franqueado.php">Seja Franqueado</a>
            </div>-->
        </li>
        <li>
            <a href="seja-franqueado.php">
                <img src="images/8.png" alt=""/>
                <span class="sdt_active"></span>
                        <span class="sdt_wrap">
                            <span class="sdt_link">Franqueado</span>
                            <span class="sdt_descr">Seja um Franqueado!</span>
                        </span>
            </a>
        </li>
    </ul>
</div>

<!-- The JavaScript -->
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
<script type="text/javascript" src="jquery.easing.1.3.js"></script>
<script type="text/javascript">
    $(function() {
        /**
         * for each menu element, on mouseenter,
         * we enlarge the image, and show both sdt_active span and
         * sdt_wrap span. If the element has a sub menu (sdt_box),
         * then we slide it - if the element is the last one in the menu
         * we slide it to the left, otherwise to the right
         */
        $('#sdt_menu > li').bind('mouseenter',function(){
            var $elem = $(this);
            $elem.find('img')
                .stop(true)
                .animate({
                    'width':'170px',
                    'height':'170px',
                    'left':'0px'
                },400,'easeOutBack')
                .andSelf()
                .find('.sdt_wrap')
                .stop(true)
                .animate({'top':'140px'},500,'easeOutBack')
                .andSelf()
                .find('.sdt_active')
                .stop(true)
                .animate({'height':'170px'},300,function(){
                    var $sub_menu = $elem.find('.sdt_box');
                    if($sub_menu.length){
                        var left = '170px';
                        if($elem.parent().children().length == $elem.index()+1)
                            left = '-170px';
                        $sub_menu.show().animate({'left':left},200);
                    }
                });
        }).bind('mouseleave',function(){
            var $elem = $(this);
            var $sub_menu = $elem.find('.sdt_box');
            if($sub_menu.length)
                $sub_menu.hide().css('left','0px');

            $elem.find('.sdt_active')
                .stop(true)
                .animate({'height':'0px'},300)
                .andSelf().find('img')
                .stop(true)
                .animate({
                    'width':'0px',
                    'height':'0px',
                    'left':'85px'},400)
                .andSelf()
                .find('.sdt_wrap')
                .stop(true)
                .animate({'top':'25px'},500);
        });
    });
</script>