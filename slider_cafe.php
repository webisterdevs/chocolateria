<div id="slider">
    <figure>
        <img src="_img/slide_cafe1_peq.jpg">
        <img src="_img/slide-loja_2.png" alt="">
        <img src="https://fbcdn-sphotos-g-a.akamaihd.net/hphotos-ak-xaf1/v/t1.0-9/11898535_448602098655725_862451785458365191_n.jpg?oh=fe22a13de56a7cc841b1e46a3616c422&oe=578F276E&__gda__=1468974608_6217014863bb2cb9a31d1e95c5098518" alt="">
        <img src="https://fbcdn-sphotos-d-a.akamaihd.net/hphotos-ak-xla1/v/t1.0-9/12391970_480853925430542_3073854913848725764_n.jpg?oh=84b174156eeb766d2cf2145e3a19cdde&oe=57950A19&__gda__=1468470964_e9e6a41d724084832862c1ae410f572d" alt="">
        <img src="https://scontent-mia1-1.xx.fbcdn.net/hphotos-xaf1/v/t1.0-9/11903731_450859021763366_6015515742929935328_n.jpg?oh=1dac7ec9ddb0d6a784fd0bc5e28b507b&oe=5751C3DA" alt="">
    </figure>
</div>

<style>
    @keyframes slidy {
        0% { left: 0%; }
        20% { left: 0%; }
        25% { left: -100%; }
        45% { left: -100%; }
        50% { left: -200%; }
        70% { left: -200%; }
        75% { left: -300%; }
        95% { left: -300%; }
        100% { left: -400%; }
    }

    body {
        margin: 0 8px 0 0;

    }

    div#slider {
        overflow: hidden;
        margin: -35px auto 50px auto;
        padding-bottom: 0;
        width: 70%;
        height: 550px;

    }

    div#slider figure img {
        width: 20%;
        float: left;
    }

    div#slider figure {
        position: relative;
        width: 500%;
        margin: 0;
        left: 0;
        text-align: center;
        font-size: 0;
        animation: 40s slidy infinite;
    }

</style>