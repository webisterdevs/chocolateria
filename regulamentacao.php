<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">

    <?php
    include 'menu.php';
    ?>

    <section id="corpo-lojas">

        <div id="em-breve"><h1> Regulamentação da Promoção</h1><br>
            <h4><small><i>"Dia dos Namorados Com Sabor Especial"</i></small></h4>
            <!-- <img src="_img/logo-marrom.png"></div><br><br><br>  -->
            <div class="textos">
                <br>
            <h6><b>Empresa Promotora: Chocolateria Brasieira Franquia Ltda.</b></h6>
            <h6>1-	Chocolateria Brasileira Franquia Ltda., estabelecida na Rua Antonio Vicentini nº 199 – Itatiba – SP, inscrita no CNPJ/MF sob o nº 20.489.292/0001-85, cederá gratuitamente aos clientes que preencherem as condições do regulamento da promoção o direito de participar do sorteio relativo ao referido item.<br><br>

                2-    Poderão participar desta Promoção, pessoas físicas, maiores de 18 anos, que realizarem compras acima de R$50,00 (cinqüenta reais) em uma das lojas da empresa promotora.<br><br>
                3-    A cada R$50,00 (cinqüenta reais) em compras realizadas na Chocolateria Brasileira, o cliente participante ganhará  um cupom que deverá ser totalmente preenchido de forma legível e assinalando a resposta correta: Chocolateria Brasileira e depositar na urna disponível na loja de compra ou qualquer outra da rede Chocolateria Brasileira.<br><br>
                4-    A promoção terá inicio em 01/06/2016 e término em 30/06/2016 e será realizada em todo território nacional.<br><br>
                5-    O sorteio será realizado no dia 15/07/2016, na sede da Chocolateria Brasileira na cidade de Itatiba – SP mediante envio dos cupons pela unidade franqueada e auditada pelos responsáveis pela promoção em vigor.<br><br>
                6-    Para efeito de apuração, acerca do descrito no caput deste artigo, considerar-se-ão o prêmio único através de sorteio aleatório, na presença de equipe jurídica e testemunhas.<br><br>
                7-    O item distribuído em prêmio é de 01 – bracelete Swarovski modelo Dear, livre de impostos ou qualquer ônus, conforme determina a legislação.<br><br>
                8-    A Chocolateria Brasileira Franquia Ltda efetuará a entrega do prêmio ao contemplado, no prazo máximo de 30 dias úteis, contados da data da realização do sorteio, desde que o ganhador apresente: o CPF, o comprovante de residência atualizado, emitido em data não superior a 180 dias da apresentação, a carteira de identidade e demais documentos porventura necessários à concretização do pagamento.<br><br>
                9-    O prazo para reclamação do prêmio pelo contemplado é de 5 (cinco) anos a partir do sorteio.<br><br>
                10- Só poderão participar do sorteio os clientes que atenderem as condições estabelecidas neste regulamento, ficando vedada a participação de funcionários ou franqueados da “Chocolateria Brasileira”.<br><br>
                11- O cliente contemplado autoriza, desde já, a Promotora do Evento a tornar pública a sua premiação, com a utilização do seu nome, imagem e voz, sem qualquer ônus, pelo prazo de 5 (cinco) anos.<br><br>
                12-  A autorização descrita no Item 11 ,exclusiva para este fim, não significa nem implica ou resulta em obrigatoriedade de divulgação, sendo uma faculdade da promotora.<br><br>
                13-  O contemplado será informado da premiação por meio de carta, telefone ou mídia eletrônica.<br><br>
                14- Este Regulamento será disponibilizado no site  www.chocolateriabrasileira.com.br<br><br>

                15-  A Promotora não será responsável por dados incompletos, ilegíveis ou danificados que impossibilitem a identificação do contemplado e a entrega do prêmio<br><br>
                16-  O cupom contemplado será divulgadas pelo site da empresa promotora  www.chocolateriabrasileira.com.br e redes sociais;
            </h6>
            </div>
            <br>
        </div>

    </section>

</div>

<?php
include 'rodape.php';
?>

</body>
</html>