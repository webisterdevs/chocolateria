<!DOCTYPE html>
<html>
<head lang="pt-br">
    <meta charset="UTF-8">
    <link rel="stylesheet" href="_css/estilo.css">
    <link rel="stylesheet" href="_css/fotos.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.css">
    <link rel="stylesheet" type="text/css" href="bower_components/all-animation/assets/css/all-animation.min.css">
    <link rel="stylesheet" href="css/style.css" type="text/css" media="screen"/>
    <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
    <script src="script.js"></script>
    <script type="text/javascript" src="_js/main.js"></script>
    <title>Chocolateria Brasileira</title>
</head>
<body>
<div id="fundo-outras">
    <?php
    include 'menu.php';
    ?>
    <section id="corpo-lojas">
        <h4><small><a href="produtos.php">Produtos</a> > <a href="presentes.php">Presentes</a> > Caixa Seleçoes 160gr</small></h4>

        <table width="80%" id="produto-grande" cellpadding="22px">
            <tr>
                <td>
                    <img src="_img/caixa-selecoes.png">
                </td>
                <td>
                    <h1><big>Caixa Seleçoes 160gr :</big></h1>
                    <h2>Caixa para presente com delicados bombons de chocolate ao leite, ao leite crocante com castanha de caju caramelizada e branco em uma apresentação delicada para transformar momentos simples em especiais.

                    </h2>
                </td>
            </tr>
        </table>

    </section>

</div><br><br>
<?php
include 'rodape.php';
?>
</body>
</html>